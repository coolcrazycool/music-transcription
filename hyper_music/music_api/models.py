from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.core.files.base import ContentFile

#модель мелодии для БД
class Melody(models.Model):
    STATUS = (
        ('Uploaded', 'Uploaded'),
        ('Converted', 'Converted')
    )# возможные статусы при загрузке мелодии

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True) #внешний ключ по пользователю, загружающему мелодию
    melody = models.FileField(upload_to="musics") #непосредственно файловое поле для хранения мелодии
    name = models.CharField('Название мелодии', max_length=100, default='NoName') #текстовое поле длинны 100 для названия
    status = models.CharField(max_length=200, null=True, choices=STATUS, blank=True) # текстовое поле длинны 200 для статуса
    pdf = models.FileField(upload_to="pdf", null=True, blank=True) #файловое поле для сконвертированного PDF файла

    def __str__(self): #строковым отображением мелодии является ее имя
        return self.name
