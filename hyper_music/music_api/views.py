from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from rest_framework.views import APIView
from rest_framework.response import Response

from django.contrib.auth.decorators import login_required
from hyper_music.settings import MEDIA_ROOT

# Own views are here
from .forms import MelodyForm, CreateUserForm
from .serializers import MelodySerializer
from app_code.models import Music
from app_code.models import Notes, CodeMelody
from .models import Melody
from logs.logger import decorated_log, logger_e
import os
 
#отображение домашей страницы
@login_required(login_url='login')
def home(request):
    return render(request, 'home.html')

#отображение страницы регистрации
@decorated_log
def registerPage(request):
    if request.user.is_authenticated: #если пользователь уже авторизован, редирект на домашнюю
        return redirect('home')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST) #создание формы пользователя по данным из POST запроса
            if form.is_valid(): # проверка на корректность данных
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user) #вывод сообщения об успешной регистрации пользователя
                return redirect('login')
            else:
                logger_e.warning('Ошибка при регистрации')
                messages.info(request, 'Username OR password is incorrect')

        context = {'form': form}
        return render(request, 'accounts/register.html', context) #если request.method не POST - страница регистрации

#отображение страницы авторизации
@decorated_log
def loginPage(request):
    if request.user.is_authenticated: #если пользователь уже авторизован, редирект на домашнюю
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username') #забираем данные из request.POST о логине и пароле
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password) # передаем собранные выше данные для авторицации

            if user is not None:
                login(request, user) #логиним пользователя в систему,если он существует
                return redirect('home')
            else:
                logger_e.warning('Ошибка при авторизации')
                messages.info(request, 'Username OR password is incorrect') #если пользователь не существует, сообщение о неккоректности данных
        context = {}
        return render(request, 'accounts/login.html', context) #если request.method не POST - страница авторизации

#функция для выхода из аккаунта
def logoutUser(request):
    logout(request)
    return redirect('login')

#отображение профиля пользователя
@login_required(login_url='login')
def profilePage(request):
    melodys = Melody.objects.filter(user=request.user.id) #выделение мелодий, которые относятся к конкретному пользователю
    return render(request, 'accounts/profile.html', {'melodys': melodys}) #отображение мелодий, полученных выше

#загрузка мелодии пользователем
@login_required(login_url='login')
@decorated_log
def upload(request):
    form = MelodyForm()
    if request.method == 'POST':
        form = MelodyForm(request.POST, request.FILES) #создание мелоди с загруженными пользователем файлами
        # try:
        mel = str(request.FILES['melody'])
        if form.is_ok(mel): #проверка файла с мелодией на соотвествие требованиям формата файла
            melody_pr = form.save(commit=False)
            melody_pr.melody = request.FILES['melody'] #назначение мелодии соответсвующего файла
            melody_pr.name = request.POST['name'] #назначение мелодии соответсвующего имени
            melody_pr.user = request.user #назначение мелодии пользователя, который загрузил файл
            melody_pr.status = 'Uploaded'
            form.save()
            obj = Melody.objects.latest('id')
            obj.status = 'Uploaded'
            obj.save()
            return redirect('profile') # редирект в профиль
        # except Exception as e:
        # raise forms.ValidationError('Can not identify file type')
        else:
            logger_e.warning('Файл не загружен, неверное расширение')
            messages.info(request, 'Invalid data type. Only wav') #сообщение о некорректности загруженного файла
    return render(request, 'upload.html', {'form': form}) #если request.method не POST - страница загрузки

#обработка конвертации файлов
@decorated_log
def converte(request, pk):
    melody = get_object_or_404(Melody, pk=pk) #получение мелодии, вызов get() по модели Melody, с Http404 вместо DoesNotExist exception модели
    NOTES = CodeMelody(f'{MEDIA_ROOT}/{melody.melody}') #создание мелодии по заданному пути
    data = NOTES.data #массив, описанный в models.md
    PDF = Notes(data) #создание объекта класса Notes с заданным выше массивом
    pdf = PDF.converteToPdf(f'{melody.melody}'.split('/')[-1].split('.')[0]) #конвертация в пдф
    melody.pdf.name = f'pdf/{pdf}.pdf'
    melody.status = 'Converted'
    melody.save()
    return redirect('profile')

# отображение мелодий
class MelodyView(APIView):
    def get(self, request):
        melodys = Melody.objects.all()
        serializer = MelodySerializer(melodys, many=True)
        return Response({"melodys": serializer.data})
