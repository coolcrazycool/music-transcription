import os

from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from .models import Melody

#форма для создания пользователя
class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

#форма для создания мелодии
class MelodyForm(ModelForm):
    class Meta:
        model = Melody
        fields = ['user', 'melody', 'name', 'status']

    def is_ok(self, mel): #проверка на формат файла (wav only)
        ext = str(mel).split('.')[-1]
        if ext.lower() == 'wav':
            return True
        else:
            return False
